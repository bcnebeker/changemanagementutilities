// rmlink.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

int wmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	if (argc < 2) return -1;

	if (RemoveDirectory(argv[1]) == 0)
	{
		wprintf(L"RemoveDirectory Failed error: %u\n", GetLastError());
	}


    return 0;
}

