#include "stdafx.h"
#include "FileSmartIO.h"
#include "FilePtr.h"
#include <time.h>

#define TIME_BUFF_LEN		64

CFileSmartIO::CFileSmartIO()
{
}

CFileSmartIO::~CFileSmartIO()
{
}

CFilePtr * CFileSmartIO::OpenFile(CFilename *ptrFilename, bool Updateable)
{
	// Open input and output files
	CFilePtr * ptr = new CFilePtr(this);
	ptr->Read_fp = _wfopen((const wchar_t *)ptrFilename, L"rb");
	Updateable ? ptr->Write_fp = _wfopen(MakeOutputFile(ptr, ptrFilename), L"wb") : nullptr;

	//
	// Read file encoding from source file
	//
	
	// Read the first 4 bytes of the file
	BOM Marker;
	fread((void *)&Marker, 4, 1, ptr->Read_fp);

	if (Marker.Bit32 = 0xfffe0000)
	{
		ptr->FileEncode.Encoding = EncodeType::B32;
		ptr->FileEncode.ChrSize = 32;
	}
	else if (Marker.Bit16 == 0xfffe)
	{
		ptr->FileEncode.Encoding = EncodeType::B16;
		ptr->FileEncode.ChrSize = 16;
	}
	else if ((Marker.Bit32 & 0xffffff) == 0xefbbbf)
	{
		ptr->FileEncode.Encoding = EncodeType::B8;
		ptr->FileEncode.ChrSize = 8;
	}
	else
	{
		ptr->FileEncode.Encoding = EncodeType::ASCII;
		ptr->FileEncode.ChrSize = 8;
	}

	return ptr;
}

const wchar_t * CFileSmartIO::MakeOutputFile(CFilePtr * ptrFile, CFilename *InFilename)
{
wchar_t	TimeBuff[TIME_BUFF_LEN + 2];

	// Use source filename as a starting point
	ptrFile->OutputFilename.operator=(InFilename);

	// Get current time as a string
	time_t rawtime;
	time(&rawtime);
	struct tm * timeinfo = localtime(&rawtime);

	wcsftime(TimeBuff, TIME_BUFF_LEN, L"%Y-%m-%d-%H-%M-%S", timeinfo);

	// Add new extension to make a unique filename
	ptrFile->OutputFilename += L"updated_";
	ptrFile->OutputFilename += TimeBuff;

	return ptrFile->OutputFilename;
}
