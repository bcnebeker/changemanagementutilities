#pragma once
#include <stdio.h>
#include "Filename.h"

typedef enum class EncodeType { ASCII, B8, B16, B32 } ENCODE_TYPE;

typedef struct strFileEncode
{
	strFileEncode()
	{
		ChrSize = 0;
		Encoding = EncodeType::ASCII;
	}

	ENCODE_TYPE		Encoding;
	size_t			ChrSize;
} FILE_ENCODING;


class CFilePtr
{
	friend class CFileSmartIO;

public:
	CFilePtr(CFileSmartIO *ptrParent);
	virtual ~CFilePtr();
	FILE_ENCODING *	GetEncoding();
	int				ReadLine();


protected:
	CFileSmartIO *	Parent;
	FILE *			Read_fp;
	FILE *			Write_fp;
	CFilename		OutputFilename;
	FILE_ENCODING	FileEncode;
	
};

