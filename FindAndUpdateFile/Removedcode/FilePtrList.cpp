#include "stdafx.h"
#include "FilePtrList.h"

#define MAX_FILE_OBJ	16


CFilePtrList::CFilePtrList() : CObjList(sizeof(CFilePtr *), MAX_FILE_OBJ)
{
}

CFilePtrList::~CFilePtrList()
{
}

// Add the new file pointer object to the list
SIZEINT CFilePtrList::AddFile(CFilePtr *ptr)
{
	return CObjList::Add(ptr);
}

// Find the file pointer object in the list and remove it
int	CFilePtrList::RemoveFile(CFilePtr *ptr)
{
	return CObjList::Remove(ptr);
}
