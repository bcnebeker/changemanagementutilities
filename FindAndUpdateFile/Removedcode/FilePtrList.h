#pragma once
#include "ObjList.h"
#include "FilePtr.h"

class CFilePtrList : public CObjList
{
public:
	CFilePtrList();
	virtual ~CFilePtrList();
	SIZEINT	AddFile(CFilePtr *ptr);
	int		RemoveFile(CFilePtr *ptr);
};

