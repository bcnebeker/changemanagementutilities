#pragma once
#include "Filename.h"
#include <string.h>
#include "common.h"
#include "FilePtrList.h"

#define MAX_FILE_OBJ	8



class CFileSmartIO
{
	friend class CFilePtr;
	friend class CFilePtrList;

public:
	CFileSmartIO();
	virtual ~CFileSmartIO();
	CFilePtr * OpenFile(CFilename *ptrFilename, bool Updateable);

protected:
	const wchar_t * MakeOutputFile(CFilePtr * ptrFile, CFilename *InFilename);

protected:	// Protected data
	CFilePtrList	FilePtrList;

	union BOM
	{
		UINT32	Bit32;
		UINT16	Bit16;
	};

	char		EncodeUTF8[4] = { '\xef', '\xbb', '\xbf', '\0' };
	wchar_t		EncodeUTF16 = u'\xfeff';
	char32_t	EncodeUTF32 = U'\xfeff';
};

