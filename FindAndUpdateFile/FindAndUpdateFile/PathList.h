#pragma once
#include "ObjList.h"
#include "PathName.h"
#include "Filename.h"

typedef struct str_DirectoryEntry
{
	str_DirectoryEntry(const TCHAR *rootPath = nullptr, WIN32_FIND_DATA * Entry = nullptr)
	{
		if (rootPath != nullptr  &&  Entry != nullptr)
		{
			RootPath = rootPath;
			Name = Entry->cFileName;
			Attributes = Entry->dwFileAttributes;
		}
		else
		{
			Attributes = 0;
		}
	}

	str_DirectoryEntry(str_DirectoryEntry *ptr) : RootPath(ptr->RootPath), Name(ptr->Name)
	{
		Attributes = ptr->Attributes;
	}

	str_DirectoryEntry(str_DirectoryEntry &ref) : RootPath(ref.RootPath), Name(ref.Name)
	{
		Attributes = ref.Attributes;
	}

	str_DirectoryEntry * operator = (str_DirectoryEntry *ptr)
	{
		RootPath.operator=(ptr->RootPath);
		Name.operator=(ptr->Name);
		Attributes = ptr->Attributes;
		return this;
	}

	CPathName			RootPath;
	CFilename			Name;
	DWORD				Attributes;
} DIR_INFO;

class CPathList : CObjList
{
public:
	CPathList();
	virtual ~CPathList();
	void			Clear();
	SIZEINT			Add(const TCHAR *RootPath, WIN32_FIND_DATA *Dir);
	bool			IsEmpty();
	void			Start();
	CPathName *		GetNext(CPathName * Path, DIR_INFO * DirInfo);

protected:
	size_t		Current;
};

