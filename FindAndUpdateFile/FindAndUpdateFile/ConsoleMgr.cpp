#include "stdafx.h"
#include "ConsoleMgr.h"


CConsoleMgr::CConsoleMgr()
{
	ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO   csbi;
	if (GetConsoleScreenBufferInfo(ConsoleHandle, &csbi)) DefaultConsoleAttrib = csbi.wAttributes;

}

CConsoleMgr::~CConsoleMgr()
{
	Restore();
}

void CConsoleMgr::Restore()
{
	SetConsoleTextAttribute(ConsoleHandle, DefaultConsoleAttrib);
}

void CConsoleMgr::SetColor(WORD Attrib)
{
	SetConsoleTextAttribute(ConsoleHandle, Attrib);
}