#include "stdafx.h"
#include "FlexBuffer.h"
#include "common.h"

CFlexBuffer::CFlexBuffer(const size_t chrSize, const size_t buffSize)
{
	if (chrSize < 1) ChrSize = 1;
	else ChrSize = chrSize;

	if (buffSize < 256) BuffSize = 256;
	else BuffSize = buffSize;

	ChrSize = chrSize;
	BuffSize = buffSize;
	size_t bytes = ChrSize * BuffSize;

	Buffer = malloc(bytes);
}

CFlexBuffer::~CFlexBuffer()
{
	if (Buffer != nullptr) free(Buffer);
}

char * CFlexBuffer::GetBuffer8()
{
	return (char *)Buffer;
}

wchar_t	* CFlexBuffer::GetBuffer16()
{
	return (wchar_t *)Buffer;
}

char32_t * CFlexBuffer::GetBuffer32()
{
	return (char32_t *)Buffer;
}

