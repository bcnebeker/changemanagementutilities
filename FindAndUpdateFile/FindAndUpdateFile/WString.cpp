#include "stdafx.h"
#include "WString.h"


CWString::CWString(size_t Size)
{
	if (Size < 16) BuffSize = 16;
	else if (Size > 65536) throw("Size out or range");
	else BuffSize = Size;

	Alloc(BuffSize + 2);
}

CWString::CWString(size_t Size, const wchar_t *Init)
{
	if (Size < 16) BuffSize = 16;
	else if (Size > 65536) throw("Size out or range");
	else BuffSize = Size;

	Alloc(BuffSize + 2);

	if (Init != nullptr  &&  wcslen(Init) < BuffSize) SetString(Init);
}

CWString::CWString(CWString &ref)
{
	BuffSize = ref.BuffSize;
	Alloc(BuffSize + 2);

	wcscpy_s(Buffer, BuffSize, ref.Buffer);
}

CWString::CWString(const CWString *ptr)
{
	BuffSize = ptr->BuffSize;
	Alloc(BuffSize + 2);
	
	wcscpy_s(Buffer, BuffSize, ptr->Buffer);
}

CWString::~CWString()
{
	size_t mem = (size_t)Buffer;
	if ((mem & 0xffff) == 0x02e70) 
	{
		Buffer = Buffer;
	}

	if (Buffer != nullptr) free(Buffer);
	Buffer = nullptr;
}

void CWString::Alloc(size_t Chars)
{
	ObjID = ObjCount++;

	size_t Bytes = Chars * sizeof(wchar_t);
	Buffer = (wchar_t *)malloc(Bytes);
	//Buffer[0] = '\0';
	memset(Buffer, 0, Bytes);

	size_t mem = (size_t)Buffer;
	if ((mem & 0xffff) == 0x02e70)
	{
		Buffer = Buffer;
	}

}

const wchar_t * CWString::operator = (const wchar_t *ptr)
{
	if (ptr != nullptr  &&  wcslen(ptr) < BuffSize) return SetString(ptr);
	else throw("Buffer overflow");
}

const wchar_t & CWString::operator = (const wchar_t &ref)
{
	if (wcslen(&ref) < BuffSize) return *SetString(&ref);
	else throw("Buffer overflow");
}

CWString * CWString::operator=(const CWString * ptr)
{
	return SetString(ptr);
}

CWString & CWString::operator=(const CWString & ref)
{
	return *SetString(&ref);
}

const wchar_t * CWString::operator += (const wchar_t *ptr)
{
	if (wcslen(ptr) + wcslen(Buffer) < BuffSize) return AppendString(ptr);
	return (const wchar_t *)&Buffer;
}

const wchar_t & CWString::operator += (const wchar_t &ref)
{
	if (wcslen(&ref) + wcslen(Buffer) < BuffSize) return *AppendString(&ref);
	return (const wchar_t &)*Buffer;
}

CWString * CWString::operator += (const CWString * ptr)
{
	return AppendString(ptr);
}

CWString::operator const wchar_t *()
{
	return (const wchar_t *)Buffer;
}

//
// Protected functions
//
wchar_t * CWString::SetString(const wchar_t *ptr)
{
	wcscpy_s(Buffer, BuffSize, ptr);
	return Buffer;
}

CWString * CWString::SetString(const CWString * ptr)
{
	// Make sure this object's buffer is large enough to hold the contents of the other object
	if (wcslen(ptr->Buffer) < ptr->BuffSize)
	{
		wcscpy_s(Buffer, BuffSize, ptr->Buffer);
	}
	return this;
}

wchar_t * CWString::AppendString(const wchar_t *ptr)
{
	wcscat_s(Buffer, BuffSize, ptr);
	return Buffer;
}

CWString * CWString::AppendString(const CWString * ptr)
{
	// Make sure this object's buffer is large enough to hold the contents of the other object
	if (wcslen(Buffer) + wcslen(ptr->Buffer) < ptr->BuffSize)
	{
		wcscat_s(Buffer, BuffSize, ptr->Buffer);
	}
	return this;
}

