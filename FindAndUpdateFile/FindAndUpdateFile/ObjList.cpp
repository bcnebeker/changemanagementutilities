#include "stdafx.h"
#include "ObjList.h"


CObjList::CObjList(size_t ObjSize, size_t InitSize = 4096)
{
	HoleCount = 0;
	ListUsed = 0;
	ListSize = InitSize;
	List = new void *[ListSize];
}


CObjList::~CObjList()
{
	Clear();
}

SIZEINT CObjList::Add(void *ptr)
{
	if (ListUsed >= ListSize)
	{
		// Check for holes in the list we can use
		if (HoleCount > 0)
		{
			// Search for a hole
			for (size_t Count = 0; Count < ListUsed; Count++)
			{
				if (List[Count] == nullptr)
				{
					List[Count] = ptr;		// Use the hole
					HoleCount--;
				}
			}
		}

		// Expand the size of the list

	}
	
	List[ListUsed] = ptr;

	return ListUsed++;
}

int CObjList::Remove(void *ptr)
{
	if (ListUsed == 0) return -1;		// Item not found

	// Search the list for the object pointer
	size_t Count;
	for (Count = 0; Count < ListUsed; Count++)
	{
		if (List[Count] == ptr)
		{
			List[Count] = nullptr;
		}
	}

	if (Count < ListUsed - 1)		// If not last item in the list then we have a hole
	{
		// unless the reset of the list are holes as well
		bool OnlyHoles = true;
		for (size_t CheckCount = Count + 1; CheckCount < ListUsed; CheckCount++)
		{
			if (List[CheckCount] != nullptr)		// Used list entry found stop searching
			{
				OnlyHoles = false;
				break;		
			}
		}

		if (OnlyHoles)
		{
			ListUsed = Count + 1;					// Reduce list size because rest of list is all holes
			HoleCount -= ListUsed - Count;
		}

		HoleCount++;							// Deletion created a hole in the list
	}
	else
	{
		ListUsed--;					// Last item in the list simply reduce the list size
	}

	return 0;		// Success
}

void CObjList::Clear()
{
	if (List != nullptr)
	{
		// Destory the list
		delete[] List;
		List = nullptr;
	}

	HoleCount = 0;
	ListSize = 0;
	ListUsed = 0;
}