#include "stdafx.h"
#include "GlobalData.h"


CGlobalData::CGlobalData()
{
	OutputFlag = true;
	VerboseFlag = false;
	WriteChangesFlag = true;
	KeepTempFileFlag = false;
	KeepBackupFlag = false;
	SkipHiddenFilesFlag = true;
	SkipHiddenDirsFlag = false;
	CaseSensitiveFlag = false;
	FilesSearched = 0;
	DirSearched = 0;
	FilesProcessed = 0;
	FilesUpdated = 0;
	FilesErrors = 0;
}

CGlobalData::~CGlobalData()
{
}
