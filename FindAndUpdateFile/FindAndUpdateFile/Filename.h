#pragma once
#include "WString.h"

class CFilename : public CWString
{
public:
	CFilename();
	CFilename(const wchar_t *Init);
	CFilename(const CWString *ptr);
	virtual ~CFilename();
	const wchar_t *	operator = (const wchar_t *ptr);
	operator const wchar_t *();
};

