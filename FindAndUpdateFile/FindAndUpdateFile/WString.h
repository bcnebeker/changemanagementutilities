#pragma once
#include "WString.h"
#include "common.h"

class CWString
{
public:
	CWString(size_t Size);
	CWString(size_t Size, const wchar_t *Init);
	CWString(CWString &ref);
	CWString(const CWString *ptr);
	virtual ~CWString();
	const wchar_t *	operator = (const wchar_t *ptr);
	const wchar_t &	operator = (const wchar_t &ref);
	CWString *	operator = (const CWString *ptr);
	CWString &	operator = (const CWString &ref);
	const wchar_t * operator += (const wchar_t *ptr);
	const wchar_t &	operator += (const wchar_t &ref);
	CWString *	operator += (const CWString *ptr);
	CWString &	operator += (const CWString &ref);
	operator const wchar_t *();



protected:
	void			Alloc(size_t Chars);
	wchar_t *		SetString(const wchar_t *ptr);
	CWString *		SetString(const CWString *ptr);
	wchar_t *		AppendString(const wchar_t *ptr);
	CWString *		AppendString(const CWString *ptr);

protected:
	wchar_t	*	Buffer;
	size_t		BuffSize;
	size_t		ObjID;
};

