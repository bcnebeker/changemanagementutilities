#pragma once
class CGlobalData
{
public:
	CGlobalData();
	~CGlobalData();

	bool	OutputFlag = true;
	bool	VerboseFlag = false;
	bool	WriteChangesFlag = true;
	bool	KeepTempFileFlag = false;
	bool	KeepBackupFlag = false;
	bool	SkipHiddenFilesFlag = true;
	bool	SkipHiddenDirsFlag = false;
	bool	CaseSensitiveFlag = false;
	size_t	FilesSearched = 0;
	size_t	DirSearched = 0;
	size_t	FilesProcessed = 0;
	size_t	FilesUpdated = 0;
	size_t	FilesErrors = 0;
};

