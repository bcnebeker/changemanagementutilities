// FindAndUpdateFile.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FileList.h"
#include "ConsoleMgr.h"
#include "GlobalData.h"

#define MAX_TEXT_ARG_LEN		120
#define VERSION					L"0.1"

// Function prototypes
void DisplayUsage();

// Global data

CGlobalData		SharedData;
CConsoleMgr		Console;

int wmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
wchar_t		DefaultPattern[] = L"*";

	//
	// Process input parameters
	//
	if (argc < 5)
	{
		DisplayUsage();
		return -1;
	}

	size_t arg = 1;
	const TCHAR *Path = argv[arg++];
	const TCHAR *PathPattern = argv[arg++];
	const TCHAR *FilePattern = argv[arg++];
	const TCHAR *SearchPattern = argv[arg++];
	const TCHAR *Replacement = nullptr;

	if (argc >= arg)
	{
		Replacement = argv[arg++];
	}
	
	for (; arg < argc; arg++)
	{
		if (_wcsicmp(argv[arg], L"/n") == 0) SharedData.WriteChangesFlag = false;
		if (_wcsicmp(argv[arg], L"/s") == 0) SharedData.OutputFlag = false;
		if (_wcsicmp(argv[arg], L"/v") == 0) SharedData.VerboseFlag = true;
		if (_wcsicmp(argv[arg], L"/b") == 0) SharedData.KeepBackupFlag = true;
		if (_wcsicmp(argv[arg], L"/t") == 0) SharedData.KeepTempFileFlag = true;
		if (_wcsicmp(argv[arg], L"/hf") == 0) SharedData.SkipHiddenFilesFlag = false;
		if (_wcsicmp(argv[arg], L"/hd") == 0) SharedData.SkipHiddenDirsFlag = false;
		if (_wcsicmp(argv[arg], L"/c") == 0) SharedData.CaseSensitiveFlag = true;
	}

	// Do paramter checking
	if (wcslen(Path) == 0) return -1;
	if (wcslen(PathPattern) == 0)
	{
		PathPattern = DefaultPattern;		// If not path search pattern use wildcard
	}

	if (wcslen(FilePattern) == 0)
	{
		FilePattern= DefaultPattern;		// If not filename search pattern use wildcard
	}

	if (_wcsicmp(SearchPattern, Replacement) == 0)
	{
		// Search text and replacement text match case insensitive
		if (wcscmp(SearchPattern, Replacement) == 0)
		{
			// Search text and replace text match case senstive
			Console.SetColor(FOREGROUND_RED);
			printf("Search text and replacement text cannot match.\n\n");
			return -1;
		}
		else if (SharedData.CaseSensitiveFlag == false)
		{
			Console.SetColor(FOREGROUND_RED);
			printf("Search text and replacement text match except for case, use /c to do a case sensitive search.\n\n");
			return -1;
		}
	}

	// Check for oversize search string
	if (wcslen(SearchPattern) >= MAX_TEXT_ARG_LEN)
	{
		Console.SetColor(FOREGROUND_RED);
		printf("Search text length must be less than %I64u characters\n\n", (UINT64)MAX_TEXT_ARG_LEN);
		return -1;
	}

	// Check for oversize replacement string
	if (wcslen(Replacement) >= MAX_TEXT_ARG_LEN)
	{
		Console.SetColor(FOREGROUND_RED);
		wprintf(L"Replacement text length must be less than %I64u characters\n\n", (UINT64)MAX_TEXT_ARG_LEN);
		return -1;
	}

	// Display a heading message
	if (SharedData.OutputFlag)
	{
		wprintf(L"FindAndUpdateFile version %s\n", VERSION);
	}

	// Create a file list object for storing files that match the path and filename patterns
	CFileList FileList(Path, PathPattern, FilePattern, true);
	FILE_ENTRY * file = nullptr;

	while(FileList.ForEach(&file))
	{
		if (SharedData.OutputFlag)
		{
			if (SharedData.VerboseFlag)
			{
				wprintf(L"\nFile: %s%s\n", (const wchar_t *)file->Path, (const wchar_t *)file->Filename);
			}
			else
			{
				wprintf(L"File: %s ", (const wchar_t *)file->Filename);
			}
		}

		if (SharedData.WriteChangesFlag  || SharedData.OutputFlag)
		{
			// Build the full filename path for the file
			wchar_t FilePathName[FILENAME_MAX];
			wcscpy_s(FilePathName, file->Path);
			wcscat_s(FilePathName, file->Filename);

			int Status = FileList.ReplacePattern(FilePathName, SearchPattern, Replacement);
			if (Status > 0)
			{
				SharedData.FilesUpdated++;

				// Store updated file
				if (SharedData.OutputFlag)
				{
					wprintf(L"updated.\n");
				}
			}
			else if (Status == 0)
			{
				if (SharedData.OutputFlag)
				{
					wprintf(L"not updated.\n");
				}
			}
			else
			{
				SharedData.FilesErrors++;

				if (SharedData.OutputFlag)
				{
					wprintf(L"error updating.\n");
				}

			}
		}
	};

	if (SharedData.OutputFlag)
	{
		if (SharedData.VerboseFlag == false)
		{
			wprintf(L"\n%I64u files processed. %I64u files updated.", (UINT64)FileList.MatchCount, (UINT64)SharedData.FilesUpdated);
		}
		else
		{
			wprintf(L"\n%I64u directories and %I64u files searched.\n", (UINT64)SharedData.DirSearched, (UINT64)SharedData.FilesSearched);
			wprintf(L"%I64u files processed. %I64u files updated.\n", (UINT64)FileList.MatchCount, (UINT64)SharedData.FilesUpdated);
		}
	}

	return 0;
}

void DisplayUsage()
{
	Console.SetColor(FOREGROUND_RED);
	printf("\nFindReplace help\n\n");
	printf("Arg 1 - Path to start searching\n");
	printf("Arg 2 - Path match pattern. * to match any path or specify a path pattern.\n");
	printf("Arg 3 - Filename match pattern.\n");
	printf("Arg 4 - String to replace.\n");
	printf("Arg 5 - Replacement string.\n\n");
	printf("Wildcards ? - matches any single character, matches 1 or more characters and any value.\n");
	printf("Optional Flags: /n, /s, /v, /t, /b, /h\n");
	printf("Flag /n - Does not write changes to files.\n");
	printf("Flag /s - Silent mode, not output to the screen.\n");
	printf("Flag /v - Verbose mode, extra detail send to the screen.\n");
	printf("Flag /t - Temporary files used to process files are not erased.\n");
	printf("Flag /b - Saves a copy of all updated files.\n");
	printf("Flag /hf - Enables processing of hidden files.\n");
	printf("Flag /hd - Enables processing of hidden directories.\n");
	printf("Flag /c - Makes text replacement in files case sensitive.\n");

	Console.Restore();
}