#pragma once

class CFlexBuffer
{
public:
	CFlexBuffer(const size_t chrSize, const size_t buffSize);
	~CFlexBuffer();
	char *		GetBuffer8();
	wchar_t *	GetBuffer16();
	char32_t *	GetBuffer32();


protected:
	size_t		ChrSize;
	size_t		BuffSize;
	void *		Buffer;
};

