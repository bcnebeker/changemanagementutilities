#pragma once
#include "stdafx.h"
#include <Windows.h>

class CConsoleMgr
{
public:
	CConsoleMgr();
	~CConsoleMgr();
	void SetColor(WORD Attrib);
	void Restore();

protected:
	HANDLE	ConsoleHandle;
	WORD	DefaultConsoleAttrib;

};

