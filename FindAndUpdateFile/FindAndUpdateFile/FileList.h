#pragma once
#include "common.h"
#include "ObjList.h"
#include "string.h"
#include "windows.h"
#include "PathList.h"
#include <regex> 

#define PATH_FIRST			0
#define PATH_LAST			-9999


typedef struct strFileInfo
{
	strFileInfo(const wchar_t *path, const wchar_t *filename) : Path(path), Filename(filename)
	{
	}

	strFileInfo(const CWString *path, const CWString *filename) : Path(path), Filename(filename)
	{
	}

	CPathName	Path;
	CFilename	Filename;
	bool		Selected;
}
FILE_ENTRY;

class CFileList : CObjList
{
public:
	CFileList(const wchar_t *RootPath, const wchar_t *PathPattern, const wchar_t *FilePattern, bool SubDir);
	virtual ~CFileList();
	int		SearchFiles(const wchar_t *RootPath, const wchar_t *PathPattern, const wchar_t *FilePattern, bool SubDir);
	SIZEINT	AddFile(const wchar_t *Path, const wchar_t *Filename);
	bool	ForEach(FILE_ENTRY ** file);
	int		ReplacePattern(const wchar_t * FilePathName, const wchar_t * Pattern, const wchar_t *ReplacementValue);

protected:
	int		SearchFiles(const wchar_t *RootPath, bool SubDir);
	bool	CheckPathPattern(const wchar_t *Pattern, const wchar_t *Path);
	bool	CompareFilenamePattern(const wchar_t *Pattern, const wchar_t *Filename);
	bool	CheckFilenamePattern(const wchar_t *Filename, const wchar_t *rawPattern, const wchar_t *CurPath);
	int		ReplaceFilenamePattern(wchar_t * OutPattern, const wchar_t *Pattern, const wchar_t *ReplaceString, const wchar_t *ReplaceValue);
	int		ReplaceFilenamePatternDirectories(wchar_t * OutString, const wchar_t *String, const wchar_t *Path);
	int		ExtractDirForPath(const wchar_t *CurPath, wchar_t *DirName, SIZEINT PathNum);
	int		wcsCmpToken(const wchar_t *String, const wchar_t *Token, bool Case = true);
	bool	IsPathSeperator(wchar_t chr);
	int		CopyStringToUpper(const wchar_t *Dest, size_t DestBufferSize, const wchar_t *Src);
	const wchar_t * FindString(const wchar_t *String, const wchar_t *Pattern, size_t MatchLength);

public:
	size_t			Current;
	size_t			MatchCount;

protected:
	CPathName		PathPattern;
	CFilename		FilePattern;
};

