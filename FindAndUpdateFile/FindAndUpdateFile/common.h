#pragma once
#include "windows.h"

#ifndef _WIN64
#define SIZEINT	int
#else
#define SIZEINT	_int64
#endif

static size_t ObjCount = 0;
