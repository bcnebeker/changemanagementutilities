#include "stdafx.h"
#include "PathList.h"


CPathList::CPathList() : CObjList(sizeof(DIR_INFO), 4096)
{
}

CPathList::~CPathList()
{
	Clear();
}

void CPathList::Start()
{
	Current = 0;
}

void CPathList::Clear()
{
	// Destroy all objects in the list
	for (size_t Index = 0; Index < ListUsed; Index++)
	{
		if (List[Index] != nullptr) delete (DIR_INFO *)List[Index];
	}

	CObjList::Clear();
}

CPathName * CPathList::GetNext(CPathName * Path, DIR_INFO * DirInfo = nullptr)
{
	// If no more entries are available return null
	if (Current >= ListUsed) return nullptr;

	DIR_INFO *Item = (DIR_INFO *)List[Current];

	// If caller provided a DIR_INFO buffer then return the DIR_INFO data for the entry
	if (DirInfo != nullptr) DirInfo->operator = (Item);

	// If caller provided a Path buffer then Return a full path string result
	Path->operator=(Item->RootPath);
	Path->operator+=((CWString *)&Item->Name);
	Current++;
	return Path;
}


bool CPathList::IsEmpty()
{
	return (ListUsed == 0);
}

SIZEINT CPathList::Add(const TCHAR *RootPath, WIN32_FIND_DATA *Dir)
{
	DIR_INFO *DirInfo = new DIR_INFO(RootPath, Dir);
	return CObjList::Add(DirInfo);
}