#pragma once
#include "Filename.h"

#define TIME_BUFF_LEN		64

typedef enum class EncodeType { SKIP, ASCII, B8, B16le, B16be, B32le, B32be } ENCODE_TYPE;

class CFileProcessor
{
public:
	CFileProcessor();
	virtual ~CFileProcessor();
	int			ProcessFile(const wchar_t *ptrFilename, const wchar_t *ptrPattern, const wchar_t *ptrReplaceValue);
	char *		FindString(const char *String, const char *Pattern, bool CaseSensitiveFlag);
	wchar_t *	FindString(const wchar_t *String, const wchar_t *Pattern, bool CaseSensitiveFlag);

protected:
	int			ProcessFilePtr(FILE *Input_fp, FILE * Output_fp, const wchar_t *ptrPattern, const wchar_t *ptrReplaceValue);
	int			ProcessFilePtr(FILE *Input_fp, FILE * Output_fp, const char *ptrPattern, const char *ptrReplaceValue);
	const wchar_t * MakeOutputFilename(wchar_t *OutFilename, size_t BuffSize, const wchar_t *InFilename);
	int			WideToAscii(char *Output, size_t OutSize, const wchar_t *Input, bool IgnoreHighByte);
	ENCODE_TYPE	GetFileEncoding(FILE *Input_fp, size_t PatternLen);
	void		DisplayTextDifference(const char *OrigText, const char *NewText, size_t PatternLen, size_t ReplaceLen);
	void		DisplayTextDifference(const wchar_t *OrigText, const wchar_t *NewText, size_t PatternLen, size_t ReplaceLen);

	typedef union unBOM
	{
		UINT32	Bit32;
		UINT16	Bit16;
	} BOMBytes;

	char		EncodeUTF8[4] = { '\xef', '\xbb', '\xbf', '\0' };
	wchar_t		EncodeUTF16 = u'\xfeff';
	char32_t	EncodeUTF32 = U'\xfeff';

};

