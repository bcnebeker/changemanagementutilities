#pragma once
#include "common.h"

class CObjList
{
public:
	CObjList(size_t ObjSize, size_t InitSize);
	virtual ~CObjList();
	SIZEINT		Add(void *);
	int			Remove(void *);
	void		Clear();

protected:
	void **		List;
	size_t		ListSize;
	size_t		ListUsed;
	size_t		HoleCount;
};

