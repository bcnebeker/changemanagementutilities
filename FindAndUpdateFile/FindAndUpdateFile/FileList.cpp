#include "stdafx.h"
#include "FileList.h"
#include "FileProcessor.h"
#include "GlobalData.h"

using namespace std;

#define PATH_SEP_CHAR		L'\\'
#define PATH_SEP_LIST		L"\\/"


extern CGlobalData SharedData;



CFileList::CFileList(const wchar_t *RootPath = nullptr, const wchar_t *pathPattern = nullptr, const wchar_t *filePattern = nullptr, bool SubDir = false) : CObjList(sizeof(FILE_ENTRY), 16384)
{
	MatchCount = 0;

	if (RootPath != nullptr) SearchFiles(RootPath, pathPattern, filePattern, SubDir);
}

CFileList::~CFileList()
{
}

//
// This function search a file path for files that match both path and filename patterns.
//

int CFileList::SearchFiles(const wchar_t * RootPath, const wchar_t *pathPattern, const wchar_t *filePattern, bool SubDir)
{
	PathPattern.operator=(pathPattern);
	FilePattern.operator=(filePattern);
	if (RootPath != nullptr) return SearchFiles(RootPath, SubDir);
	else return -1;
}

//
// This function is used for recursive directory searching.
// Do not call directly.
//
int CFileList::SearchFiles(const wchar_t *RootPath, bool SubDir)
{
wchar_t			Path[MAX_PATH];
CPathName		NewPath;
wchar_t			Search[MAX_PATH];
DIR_INFO		NewFileData;
WIN32_FIND_DATA	FileData;
CPathList		PathList;

	// Get a the starting path for searching and make sure it includes a terminating back slash
	if (RootPath == nullptr) GetCurrentDirectory(MAX_PATH, Path);
	else wcsncpy_s(Path, RootPath, FILENAME_MAX - 2);

	SIZEINT Len = wcsnlen(Path, MAX_PATH);
	if (Len == 0) throw("Invalid Directory Name");
	if (Len >= MAX_PATH - 4) throw("Directory path name too long.");
	if (Path[Len - 1] != '\\')
	{
		wcscat_s((wchar_t *)&Path, MAX_PATH, L"\\");
	}

	wcscpy_s(Search, MAX_PATH, Path);
	wcscat_s(Search, MAX_PATH, L"*");

	// Get a handle for file searching
	HANDLE hFind = FindFirstFile(Search, &FileData);
	if (hFind == INVALID_HANDLE_VALUE) return 0;

	do
	{
		// Find directories
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// Filter out parent link directories
			// Skip all directories if SubDir searching is not enabled
			// Skip hidden directories if disabled
			if (wcscmp(FileData.cFileName, L".")  &&  wcscmp(FileData.cFileName, L".."))
			{
				if (SubDir && ((FileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) || (SharedData.SkipHiddenDirsFlag == false))
				{
					// Save directory for search after files are completed
					PathList.Add(Path, &FileData);
					SharedData.DirSearched++;
				}
			}
		}
		else
		{
			if (CheckPathPattern(PathPattern, Path))
			{
				if (SharedData.SkipHiddenFilesFlag == false || ( (FileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0))
				{
					SharedData.FilesSearched++;

					if (CheckFilenamePattern(FileData.cFileName, FilePattern, Path))
					{
						AddFile(Path, FileData.cFileName);
					}
				}
			}
		}
	} while (FindNextFile(hFind, &FileData));

	// Close the file seach handle
	FindClose(hFind);

	if (PathList.IsEmpty() == false)
	{
		CPathName *ptrNewPath = nullptr;
		PathList.Start();

		while ((ptrNewPath = PathList.GetNext(&NewPath, &NewFileData)) != nullptr) 
		{
			const wchar_t *ptr = ptrNewPath->operator const wchar_t *();
			SearchFiles(ptr, SubDir);
		}
	}

	return 1;
}

int	CFileList::ReplacePattern(const wchar_t * FilePathName, const wchar_t * Pattern, const wchar_t *ReplacementValue)
{
	CFileProcessor FileProc;
	
	return FileProc.ProcessFile(FilePathName, Pattern, ReplacementValue);
}

//
// This function checks if a given filename matches the filename pattern provided by the user
//
bool CFileList::CheckFilenamePattern(const wchar_t *Filename, const wchar_t *rawPattern, const wchar_t *CurPath)
{
static wchar_t Pattern[FILENAME_MAX + 2];
static wchar_t PrevPath[MAX_PATH + 2];
static bool FirstPass = true;
int		Status;
	
	if (FirstPass || wcscmp(CurPath, PrevPath))
	{
		FirstPass = false;
		wcscpy_s(PrevPath, CurPath);

		// Process any special imbedded patterns
		Status = ReplaceFilenamePatternDirectories((wchar_t *)&Pattern, rawPattern, CurPath);
		if (Status < 0) throw("Invalid string found.");
	}

	bool bStatus = CompareFilenamePattern(Pattern, Filename);
	if (bStatus)
	{
		++MatchCount;
	}
	
	return bStatus;
}

bool CFileList::CheckPathPattern(const wchar_t *Pattern, const wchar_t *Path)
{
	return CompareFilenamePattern(Pattern, Path);
}

bool CFileList::CompareFilenamePattern(const wchar_t *Pattern, const wchar_t *Filename)
{
	wchar_t	PatternBuff[MAX_PATH];
	wchar_t	FilenameBuff[FILENAME_MAX];
	wchar_t *PatternPos = PatternBuff;
	wchar_t *FilenamePos = FilenameBuff;

	// Error check input
	if (Pattern == nullptr) throw "Invalid parameter";
	if (Filename == nullptr) throw "Invalid parameter";

	// Copy input strings to buffers and convert contents to upper case
	if (CopyStringToUpper(PatternBuff, MAX_PATH, Pattern) < 0) throw "Buffer Overflow";
	if (CopyStringToUpper(FilenameBuff, FILENAME_MAX, Filename) < 0) throw "Buffer Overflow";

	while (*PatternPos != '\0'  &&  *FilenamePos != '\0')
	{
		if (*PatternPos == '?')
		{
			++PatternPos;
			++FilenamePos;
		}
		else if (*PatternPos == '*')
		{
			size_t MatchLength = 0;
			wchar_t * SavePos = ++PatternPos;

			// Determine the number of characters in the pattern that must match in the filename
			while (*PatternPos != '*'  &&  *PatternPos != '?'  &&  *PatternPos != '\0')
			{
				MatchLength++;
				PatternPos++;
			}
			
			// If * is end of pattern then nothing left to compare
			if (MatchLength == 0 && *PatternPos == '\0') return true;

			// Do comparision
			const wchar_t * Str = FindString(FilenamePos, SavePos, MatchLength);
			if (Str == nullptr) return false;		// No match

			// Adjust string pointers to end of match
			FilenamePos = (wchar_t *) Str + MatchLength;
		}
		else
		{
			if (*PatternPos++ != *FilenamePos++) return false;
		}
	}

	// Check for rest of pattern is wild cards and at end the filename
	if (*FilenamePos == '\0'  &&  *PatternPos != '\0')
	{
		while (*PatternPos != '\0' && (*PatternPos == '?' || *PatternPos == '*')) ++PatternPos;
	}

	// Check for pattern or filename did not compare to end of string
	if (*PatternPos != '\0' || *FilenamePos != '\0') return false;

	return true;		// Matched
}

int CFileList::CopyStringToUpper(const wchar_t *DestBuff, size_t DestBufferSize, const wchar_t *SrcBuff)
{
	wchar_t * Dest = (wchar_t *)DestBuff;
	wchar_t * Src = (wchar_t *)SrcBuff;

	// Make sure the destination buffer is large enough
	size_t Len = wcslen(Src);
	if (Len >= DestBufferSize) return -1;

	// Copy the characters converted to upper case
	while (*Src != '\0') *Dest++ = toupper(*Src++);
	*Dest = '\0';		// Terminate the destination string
	return 0;
}

const wchar_t * CFileList::FindString(const wchar_t *String, const wchar_t *Pattern, size_t MatchLength)
{
	// Copy the comparision string into a buffer
	wchar_t *Buff = new wchar_t[MatchLength + 2];
	wcsncpy_s(Buff, MatchLength + 2, Pattern, MatchLength);

	// Find the pattern
	const wchar_t * Result = wcsstr(String, Buff);
	
	delete[] Buff;	// Free the buffer

	return Result;
}


//
// This function check a pattern for embedded special constants and replaces them with values
// related to the constant.
//
int CFileList::ReplaceFilenamePattern(wchar_t * OutString, const wchar_t *String, const wchar_t *Token, const wchar_t *ReplaceValue)
{
	wchar_t *		Pos;
	bool		Changed = false;

	size_t StringLen = wcslen(String);
	size_t TokenLen = wcslen(Token);
	size_t ReplaceLen = wcslen(ReplaceValue);
	size_t Avail = FILENAME_MAX - StringLen;		// Get the amount of buffer space we have in excess of the input string
	SIZEINT Req = (int)ReplaceLen - TokenLen;		// Calc how much bigger or smaller the replacement string is to the pattern string

	wcscpy_s(OutString, FILENAME_MAX, String);

	while ( (Pos = wcsstr(OutString, Token)) )
	{
		// Make sure we have room in the buffer for the new string content
		// AvailableSpace - RequiredSpace must be > 0
		if ((Avail - Req) > 0)
		{
			// replace the detected pattern with the replacement string
			wcsncpy_s(Pos, Avail, ReplaceValue, ReplaceLen);

			// Move original string content to the end of the matched pattern in the buffered string
			size_t offset = Pos - OutString;					// Calc offset into the buffer when match occurred
			wcscpy_s(OutString + offset + ReplaceLen, Avail, String + offset + TokenLen);

			Avail -= Req;
			Changed = true;
		}
		else
		{
			return -1;			// Not enough buffer space to make the replacement
		}
	}

	return Changed ? 1 : 0;		// Success
}


int CFileList::ReplaceFilenamePatternDirectories(wchar_t * OutString, const wchar_t *String, const wchar_t *Path)
{
size_t	TokenLen;
size_t	ReplaceLen;
SIZEINT	Req;
wchar_t	ReplaceValue[MAX_PATH];
const wchar_t Token[] = L"<~CURDIR";
const wchar_t TokenEnd[] = L"~>";
wchar_t 	TokenVar[64];
wchar_t *	Pos;

	TokenVar[0] = '\0';

	bool Changed = false;

	size_t StringLen = wcslen(String);
	size_t Avail = FILENAME_MAX - StringLen;		// Get the amount of buffer space we have in excess of the input string
	size_t TokenEndLength = wcslen(TokenEnd);

	wcscpy_s(OutString, FILENAME_MAX, String);

	while ((Pos = wcsstr(OutString, Token)))
	{
		// Token found
		// find end of token and get token variant
		wchar_t * EndToken = wcsstr(Pos, TokenEnd);
		if (EndToken == nullptr) return 0;			// End of token not found ignore token

		// Extract token variant
		TokenLen = wcslen(Token);
		size_t TokenVarLen = EndToken - Pos - TokenLen;
		size_t BuffSize = sizeof(TokenVar) / sizeof(wchar_t);
		wchar_t *Src = Pos + TokenLen;
		wcsncpy_s(TokenVar, BuffSize, Src, TokenVarLen);
		int Offset = _wtoi(TokenVar);

		if (wcslen(TokenVar) == 0)
		{
			wcscpy_s(ReplaceValue, MAX_PATH, Path);
		}
		else
		{
			if (wcsCmpToken(TokenVar, L"_FIRST") == 0)
			{
				ExtractDirForPath(Path, ReplaceValue, PATH_FIRST);
			}
			else if (wcsCmpToken(TokenVar, L"_LAST") == 0)
			{
				ExtractDirForPath(Path, ReplaceValue, PATH_LAST);
			}
			else
			{
				ExtractDirForPath(Path, ReplaceValue, Offset);
			}
		}

		ReplaceLen = wcslen(ReplaceValue);
		Req = (int)ReplaceLen - TokenLen;		// Calc how much bigger or smaller the replacement string is to the pattern string

		// Make sure we have room in the buffer for the new string content
		// AvailableSpace - RequiredSpace must be > 0
		if ((Avail - Req) > 0)
		{
			// replace the detected pattern with the replacement string
			wcsncpy_s(Pos, Avail, ReplaceValue, ReplaceLen);

			// Move original string content to the end of the matched pattern in the buffered string
			size_t offset = Pos - OutString;					// Calc offset into the buffer when match occurred
			wcscpy_s(OutString + offset + ReplaceLen, Avail, String + offset + TokenLen + TokenVarLen + TokenEndLength);

			Avail -= Req;
			Changed = true;
		}
		else
		{
			return -1;			// Not enough buffer space to make the replacement
		}
	}

	return Changed ? 1 : 0;		// Success
}

SIZEINT CFileList::AddFile(const wchar_t *Path, const wchar_t *Filename)
{
	FILE_ENTRY *Entry = new FILE_ENTRY(Path, Filename);
	return CObjList::Add(Entry);
}

bool CFileList::ForEach(FILE_ENTRY ** file)
{
	if (file == nullptr) throw;
	if (*file == nullptr)
	{
		Current = 0;
	}

	if (Current >= ListUsed)
	{
		*file = nullptr;
		return false;
	}

	*file = (FILE_ENTRY *)List[Current++];
	return true;
}

int CFileList::ExtractDirForPath(const wchar_t *Path, wchar_t *DirName, SIZEINT PathNum)
{
	wchar_t	PathBuffer[_MAX_PATH];		// Processing buffer
	wchar_t *	Dirs[256];					// Max directory elements 256
	wchar_t *	Pos = PathBuffer;			// Process buffer pointer
	size_t	Count = 0;					// # of directory elements found

	// Copy the input path into a buffer for splitting
	wcscpy_s(PathBuffer, Path);

	// Put string terminator is place of all path terminators
	Dirs[Count] = PathBuffer;

	while (*Pos != '\0')
	{
		if (IsPathSeperator(*Pos))
		{
			*Pos = '\0';
			Dirs[++Count] = ++Pos;
		}
		else ++Pos;
	}

	if (PathNum == PATH_LAST)
	{
		PathNum = Count - 1;
	}
	else if (PathNum < 0)
	{
		PathNum = Count + PathNum;
	}

	wcscpy_s(DirName, MAX_PATH, Dirs[PathNum]);

	return 0;		// Success
}

bool CFileList::IsPathSeperator(wchar_t chr)
{
	wchar_t List[] = PATH_SEP_LIST;
	wchar_t * ptr = List;

	if (chr == '\0') return false;

	while (*ptr != chr  &&  *ptr != '\0') ++ptr;
	if (*ptr == '\0') return false;
	return true;
}

int CFileList::wcsCmpToken(const wchar_t *String, const wchar_t *Token, bool Case)
{
	size_t TokenLen = wcslen(Token);

	if (Case)
	{
		return wcsncmp(String, Token, TokenLen);
	}
	else
	{
		return _wcsnicmp(String, Token, TokenLen);
	}
}