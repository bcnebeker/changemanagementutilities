#include "stdafx.h"
#include "FileProcessor.h"
#include "ConsoleMgr.h"
#include "GlobalData.h"
#include <string.h>
#include <time.h>

#define MAX_LINE_SIZE		256

extern CGlobalData		SharedData;
extern CConsoleMgr		Console;

//
// Only Ascii, UTF8, and UTF16 little endian file formats are supported
//


CFileProcessor::CFileProcessor()
{
}

CFileProcessor::~CFileProcessor()
{
}

int CFileProcessor::ProcessFile(const wchar_t *ptrFilename, const wchar_t *ptrPattern, const wchar_t *ptrReplaceValue)
{
FILE *		Input_fp = nullptr;
FILE *		Output_fp = nullptr;
wchar_t		OutFilename[FILENAME_MAX];
char		PatternAscii[MAX_LINE_SIZE + 1];
char		ReplaceValueAscii[MAX_LINE_SIZE + 1];
int			Status = 0;

	// Error check input
	if (ptrFilename == nullptr) return -1;
	if (ptrPattern == nullptr) return -1;

	// Build the filename of the output file
	MakeOutputFilename(OutFilename, FILENAME_MAX, ptrFilename);

	// Open input and output files
	Input_fp = _wfopen(ptrFilename, L"rb");
	if (Input_fp == nullptr)
	{
		int Error = GetLastError();
		if (Error == 2) return 0;		// File no longer exists
	}
	
	// Increment files processed
	SharedData.FilesProcessed++;

	// Determine file encoding
	ENCODE_TYPE FileEncoding = GetFileEncoding(Input_fp,  wcslen(ptrPattern));
	if (FileEncoding == ENCODE_TYPE::SKIP)
	{
		fclose(Input_fp);		// Close the file
		return 0;				// Return file not changed
	}

	// Open the output file
	if (SharedData.WriteChangesFlag) Output_fp = _wfopen(OutFilename, L"wb");

	// Write BOM bytes to new file if output file is open
	if (Output_fp != nullptr)
	{
		if (FileEncoding == ENCODE_TYPE::B8)
		{
			fwrite(&EncodeUTF8, 3, 1, Output_fp);
		}
		else if (FileEncoding == ENCODE_TYPE::B16le)
		{
			fwrite(&EncodeUTF16, 2, 1, Output_fp);
		}
	}

	if (FileEncoding == ENCODE_TYPE::ASCII  || FileEncoding == ENCODE_TYPE::B8)
	{		// 8-bit encoding
		// convert the 16-bit pattren to a 8-bit pattern
		Status = WideToAscii(PatternAscii, sizeof(PatternAscii), ptrPattern, true);
		if (Status < 0) throw("Buffer to small");

		// convert the 16-bit replacement value to a 8-bit pattern	
		Status = WideToAscii(ReplaceValueAscii, sizeof(ReplaceValueAscii), ptrReplaceValue, true);
		if (Status < 0) throw("Buffer to small");

		Status = ProcessFilePtr(Input_fp, Output_fp, PatternAscii, ReplaceValueAscii);
	}
	else if (FileEncoding == ENCODE_TYPE::B16le)
	{
		Status = ProcessFilePtr(Input_fp, Output_fp, ptrPattern, ptrReplaceValue);
	}
	else
	{
		// Unsupported encoding
		Status = -1;
	}

	fclose(Input_fp);
	if (Output_fp != nullptr)
	{
		fclose(Output_fp);
		Output_fp = nullptr;
	}

	// Replace original file with updated version if file was modified
	if (Status > 0)
	{
		if (SharedData.KeepTempFileFlag == false)
		{
			if (SharedData.KeepBackupFlag)	// Keep backup of orginal file
			{
				// Rename the original file
				wchar_t BackupFilename[FILENAME_MAX];

				wcscpy_s(BackupFilename, FILENAME_MAX, ptrFilename);
				wcscat_s(BackupFilename, FILENAME_MAX, L".bak");

				// Remove any existing backup file
				_wremove(BackupFilename);

				int Status = _wrename(ptrFilename, BackupFilename);
				if (Status) Status = errno;

				// Hide the backup file
				SetFileAttributes(BackupFilename, FILE_ATTRIBUTE_HIDDEN);
			}
			else
			{
				// Remove the original file
				DeleteFile(ptrFilename);
			}

			// Rename new file to original file name
			if (_wrename(OutFilename, ptrFilename) != 0)
			{
				throw("File renamed failed");
			}
		}
		else  // Output and original file kept
		{
			// Hide output file
			SetFileAttributes(OutFilename, FILE_ATTRIBUTE_HIDDEN);
		}
	}
	else
	{
		_wremove(OutFilename);
	}

	return Status;
}

int CFileProcessor::ProcessFilePtr(FILE *Input_fp, FILE * Output_fp, const wchar_t *ptrPattern, const wchar_t *ptrReplaceValue)
{
wchar_t		Buffer[MAX_LINE_SIZE * 2];
wchar_t		CopyBuffer[MAX_LINE_SIZE * 2];
bool		ModifiedFlag = false;

	// Determine how much addition buffer space is needed to have the replacement value in place of the pattern value
	size_t ReplaceLen = wcslen(ptrReplaceValue) * sizeof(wchar_t);
	size_t PatternLen = wcslen(ptrPattern) * sizeof(wchar_t);
	size_t BuffSpaceAvail = MAX_LINE_SIZE * 2 - 2 - PatternLen;
	size_t ReplaceReq = ReplaceLen > PatternLen ? ReplaceLen - PatternLen : 0;

	while (!feof(Input_fp))
	{
		if (fgetws(Buffer, MAX_LINE_SIZE, Input_fp) != nullptr)
		{
			// Save a copy of the orginal line for output to screen
			if (SharedData.OutputFlag)
			{
				wcscpy(CopyBuffer, Buffer);
			}

			// Search for string to replace
			wchar_t *Pos;
			while ((Pos = FindString(Buffer, ptrPattern, SharedData.CaseSensitiveFlag)) != nullptr)
			{
				ModifiedFlag = true;

				// Make sure our buffer can hold another replacement
				if (BuffSpaceAvail > ReplaceReq)
				{
					BuffSpaceAvail -= ReplaceReq;	// Adjust for space used

					//
					// Replace the string
					//

					// If replacement is shorter then or same length copy replacement over pattern
					if (ReplaceReq == 0)
					{
						memcpy(Pos, ptrReplaceValue, ReplaceLen);

						// If replacement is shorter then move character down in the buffer 
						if (ReplaceLen < PatternLen)
						{
							wcsncpy(Pos + ReplaceLen, Pos + PatternLen, ReplaceLen);
						}
					}
					else // Replacement is longer than the pattern make some space for the replacement by move characters down
					{
						// Start of end of string and move backward to the point of match
						wchar_t *SrcPtr = Pos + wcslen(Pos) - 1;
						wchar_t *DstPtr = SrcPtr + ReplaceReq;

						*(DstPtr + 1) = '\0';			// Write new end of line character
						do
						{
							*DstPtr-- = *SrcPtr--;
						} while (SrcPtr > Pos);

						// Copy in replacment value
						memcpy(Pos, ptrReplaceValue, ReplaceLen);
					}

					// Write replacement to screen if enabled
					if (SharedData.OutputFlag)
					{
//						wprintf(L"Replacing Line\n>%s\n with\n>%s\n\n", CopyBuffer, Buffer);
						DisplayTextDifference(CopyBuffer, Buffer, PatternLen, ReplaceLen);
					}
				}
			}
		}

		// Write the line without changes
		size_t Bytes = wcslen(Buffer) * sizeof(wchar_t);
		if (Output_fp != nullptr) fwrite(Buffer, 1, Bytes, Output_fp);
	}

	return ModifiedFlag ? 1 : 0;
}

int CFileProcessor::ProcessFilePtr(FILE *Input_fp, FILE * Output_fp, const char *ptrPattern, const char *ptrReplaceValue)
{
char		Buffer[MAX_LINE_SIZE * 2];
char		CopyBuffer[MAX_LINE_SIZE + 1];
bool		ModifiedFlag = false;

	// Determine how much addition buffer space is needed to have the replacement value in place of the pattern value
	size_t ReplaceLen = strlen(ptrReplaceValue);
	size_t PatternLen = strlen(ptrPattern);
	size_t BuffSpaceAvail = MAX_LINE_SIZE * 2 - 2 - PatternLen;
	size_t ReplaceReq = ReplaceLen > PatternLen ? ReplaceLen - PatternLen : 0;

	while (!feof(Input_fp))
	{
		if (fgets(Buffer, MAX_LINE_SIZE, Input_fp) != nullptr)
		{
			// Save a copy of the orginal line for output to screen
			if (SharedData.OutputFlag)
			{
				strcpy(CopyBuffer, Buffer);
			}

			// Search for string to replace
			char *Pos;
			while ((Pos = FindString(Buffer, ptrPattern, SharedData.CaseSensitiveFlag)) != nullptr)
			{
				ModifiedFlag = true;

				// Make sure our buffer can hold another replacement
				if (BuffSpaceAvail > ReplaceReq)
				{
					BuffSpaceAvail -= ReplaceReq;	// Adjust for space used
					
					//
					// Replace the string
					//

					// If replacement is shorter then or same length copy replacement over pattern
					if (ReplaceReq == 0)
					{
						memcpy(Pos, ptrReplaceValue, ReplaceLen);

						// If replacement is shorter then move character down in the buffer 
						if (ReplaceLen < PatternLen)
						{
							strncpy(Pos + ReplaceLen, Pos + PatternLen, ReplaceLen);
						}
					}
					else // Replacement is longer than the pattern make some space for the replacement by move characters down
					{
						// Start of end of string and move backward to the point of match
						char *SrcPtr = Pos + strlen(Pos) - 1;
						char *DstPtr = SrcPtr + ReplaceReq;

						*(DstPtr + 1) = '\0';			// Write new end of line character
						do
						{
							*DstPtr-- = *SrcPtr--;
						} while (SrcPtr > Pos);

						// Copy in replacment value
						memcpy(Pos, ptrReplaceValue, ReplaceLen);
					}
				
					// Write replacement to screen if enabled
					if (SharedData.OutputFlag)
					{
//						printf("Replacing Line\n>%s\n with\n>%s\n\n", CopyBuffer, Buffer);
						DisplayTextDifference(CopyBuffer, Buffer, PatternLen, ReplaceLen);
					}
				}
			}
		}

		// Write the line without changes
		size_t Bytes = strlen(Buffer);
		if (Output_fp != nullptr) fwrite(Buffer, 1, Bytes, Output_fp);
	}

	return ModifiedFlag ? 1 : 0;
}

wchar_t * CFileProcessor::FindString(const wchar_t *String, const wchar_t *Pattern, bool CaseSensitiveFlag)
{
wchar_t *	StringPtr;
wchar_t *	PatternPtr;
size_t	MatchLen = wcslen(Pattern);
size_t	StringLen = wcslen(String);

	if (String == nullptr) return nullptr;
	if (Pattern == nullptr) return nullptr;

	StringPtr = (wchar_t *)String;

	while (*StringPtr != '\0')
	{
		PatternPtr = (wchar_t *)Pattern;		// Start of begin of pattern

		// Find first character of Pattern
		while (toupper(*StringPtr) != toupper(*Pattern) && *StringPtr != '\0') ++StringPtr;

		// First character of pattern found
		if (*StringPtr != '\0')
		{
			// Save the current pointer for returning to the caller if a match is not made
			wchar_t *SavePtr = StringPtr;

			// Create a work pointer to try the rest of the search and move main point past first matching character
			wchar_t * tempPtr = StringPtr++;

			// Try matching the entire string now
			if (CaseSensitiveFlag)
			{
				while (*tempPtr == *PatternPtr  &&  *tempPtr != '\0'  &&  *PatternPtr != '\0')
				{
					++tempPtr;
					++PatternPtr;
				}
			}
			else
			{
				while (toupper(*tempPtr) == toupper(*PatternPtr) && *tempPtr != '\0'  &&  *PatternPtr != '\0')
				{
					++tempPtr;
					++PatternPtr;
				}
			}

			// If at end of pattern then we have a match. return saved pointer
			if (*PatternPtr == '\0')
			{
				return SavePtr;
			}
		}
	}

	return nullptr;		// No match found
}

char * CFileProcessor::FindString(const char *String, const char *Pattern, bool CaseSensitiveFlag)
{
char *	StringPtr;
char *	PatternPtr;
size_t	MatchLen = strlen(Pattern);
size_t	StringLen = strlen(String);

	if (String == nullptr) return nullptr;
	if (Pattern == nullptr) return nullptr;

	StringPtr = (char *)String;

	while (*StringPtr != '\0')
	{
		PatternPtr = (char *)Pattern;		// Start of begin of pattern

		// Find first character of Pattern
		while (toupper(*StringPtr) != toupper(*Pattern) && *StringPtr != '\0') ++StringPtr;

		// First character of pattern found
		if (*StringPtr != '\0')
		{
			// Save the current pointer for returning to the caller if a match is not made
			char *SavePtr = StringPtr;

			// Create a work pointer to try the rest of the search and move main point past first matching character
			char * tempPtr = StringPtr++;

			// Try matching the entire string now
			if (CaseSensitiveFlag)
			{
				while (*tempPtr == *PatternPtr  &&  *tempPtr != '\0'  &&  *PatternPtr != '\0')
				{
					++tempPtr;
					++PatternPtr;
				}
			}
			else
			{
				while (toupper(*tempPtr) == toupper(*PatternPtr) && *tempPtr != '\0'  &&  *PatternPtr != '\0')
				{
					++tempPtr;
					++PatternPtr;
				}
			}

			// If at end of pattern then we have a match. return saved pointer
			if (*PatternPtr == '\0')
			{
				return SavePtr;
			}
		}
	}

	return nullptr;		// No match found
}

const wchar_t * CFileProcessor::MakeOutputFilename(wchar_t *OutFilename, size_t BuffSize, const wchar_t *InFilename)
{
	wchar_t	TimeBuff[TIME_BUFF_LEN + 2];

	// Use source filename as a starting point
	wcscpy_s(OutFilename, BuffSize, InFilename);

	// Get current time as a string
	time_t rawtime;
	time(&rawtime);
	struct tm * timeinfo = localtime(&rawtime);

	wcsftime(TimeBuff, TIME_BUFF_LEN, L"%Y-%m-%d-%H-%M-%S", timeinfo);

	// Add new extension to make a unique filename
	wcscat_s(OutFilename, BuffSize, L".updated_");
	wcscat_s(OutFilename, BuffSize, TimeBuff);

	return OutFilename;
}

int CFileProcessor::WideToAscii(char *Output, size_t OutSize, const wchar_t *Input, bool IgnoreHighByte = true)
{
	int ConversionErrors = 0;

	if (OutSize <= wcslen(Input)) return -1;	// Output buffer not large enough

	// convert the 16-bit pattren to a 8-bit pattern
	char *ptr8 = Output;
	wchar_t *ptr16 = (wchar_t *)Input;

	do
	{
		if ( (IgnoreHighByte == false) && ((*ptr16 & 0xff00) != 0) )
		{
			ConversionErrors++;
		}

		*ptr8 = (*ptr16 & 0xff);
		ptr8++;
		ptr16++;
	} while (*ptr16 != '\0');
	*ptr8 = '\0';

	return ConversionErrors;		// > 0 = success but with conversion error: 0 = success not conversion errors
}

// Read the BOM bytes to determine the file encoding
EncodeType CFileProcessor::GetFileEncoding(FILE *Input_fp, size_t PatternLen)
{
BOMBytes	BOM;
ENCODE_TYPE FileType;
int			BOMLen = 0;

	// Validate input
	if (Input_fp == nullptr) throw "Invalid file pointer";

	size_t BytesRead = fread(&BOM, 1, 4, Input_fp);
	if (BytesRead == 0) return EncodeType::SKIP;

	if (BytesRead == 4)
	{
		if (BOM.Bit32 == 0x0000feff)
		{
			FileType = EncodeType::B32le;
			BOMLen = 4;
		}
		else if (BOM.Bit32 == 0x0000feff)
		{
			FileType = EncodeType::B32be;
			BOMLen = 4;
		}
		else if (BOM.Bit16 == 0xfffe)
		{
			FileType = EncodeType::B16be;
			BOMLen = 4;
		}
		else if (BOM.Bit16 == 0xfeff)
		{
			FileType = EncodeType::B16le;
			BOMLen = 2;
		}
		else if ((BOM.Bit32 & 0xffffff) == 0x00BFBBEF)
		{
			FileType = EncodeType::B8;
			BOMLen = 3;
		}
		else
		{
			FileType = EncodeType::ASCII;
			BOMLen = 0;
		}

	}
	else
	{    // Less than 4 bytes in the file
		if (BytesRead == 0 || PatternLen > BytesRead)
		{
			// File to too short to contain the pattern we are looking for no need to process the file
			FileType = EncodeType::SKIP;
			BOMLen = 0;
		}
		else if (BytesRead == 2)
		{
			// If file is 16-bit encoded then it is an empty file
			if (BOM.Bit16 == 0xfffe || BOM.Bit16 == 0xfeff)
			{
				FileType = EncodeType::SKIP;
				BOMLen = 0;
			}
			else
			{
				// The only choice left is an ascii file
				FileType = EncodeType::ASCII;
				BOMLen = 0;
			}
		}
		else if (BytesRead == 3)
		{
			// If file is utf8 encoded then it is an empty file
			if ((BOM.Bit32 & 0xffffff) == 0x00BFBBEF)
			{
				FileType = EncodeType::SKIP;
				BOMLen = 0;
			}
			else
			{
				// The only choice left is an ascii file
				FileType = EncodeType::ASCII;
				BOMLen = 0;
			}
		}
		else
		{
			// The only choice left is an ascii file
			FileType = EncodeType::ASCII;
			BOMLen = 0;
		}
	}

	// Rewind file pointer to start of file
	fseek(Input_fp, BOMLen, SEEK_SET);

	return FileType;
}

void CFileProcessor::DisplayTextDifference(const char * OrigText, const char * NewText, size_t PatternLen, size_t ReplaceLen)
{
	Console.SetColor(FOREGROUND_GREEN);
	printf("Replacing Line\n>%s\n with\n", OrigText);

	char *ptrOrig = (char *)OrigText;
	char *ptrNew = (char *)NewText;

	// Display matching text at begin of strings
	do
	{
		while (*ptrOrig == *ptrNew  &&  *ptrOrig != '\0'  &&  *ptrNew != '\0')
		{
			putchar(*ptrOrig);
			ptrOrig++;
			ptrNew++;
		}

		if (*ptrNew != '\0')
		{
			Console.SetColor(FOREGROUND_RED);
			for (size_t count = 0; count < ReplaceLen; count++) putchar(*ptrNew++);

			Console.SetColor(FOREGROUND_GREEN);
		}

		if (*ptrOrig != '\0') ptrOrig += PatternLen;
	} 
	while (*ptrOrig != '\0');

	printf("\n\n");
	Console.Restore();
}

void CFileProcessor::DisplayTextDifference(const wchar_t * OrigText, const wchar_t * NewText, size_t PatternLen, size_t ReplaceLen)
{
	Console.SetColor(FOREGROUND_GREEN);
	wprintf(L"Replacing Line\n>%s\n with\n", OrigText);

	char *ptrOrig = (char *)OrigText;
	char *ptrNew = (char *)NewText;

	// Display matching text at begin of strings
	do
	{
		while (*ptrOrig == *ptrNew  &&  *ptrOrig != '\0'  &&  *ptrNew != '\0')
		{
			putwchar(*ptrOrig);
			ptrOrig++;
			ptrNew++;
		}

		if (*ptrNew != '\0')
		{
			Console.SetColor(FOREGROUND_RED);
			for (size_t count = 0; count < ReplaceLen; count++) putchar(*ptrNew++);

			Console.SetColor(FOREGROUND_GREEN);
		}

		if (*ptrOrig != '\0') ptrOrig += PatternLen;
	} while (*ptrOrig != '\0');

	printf("\n\n");
	Console.Restore();
}
