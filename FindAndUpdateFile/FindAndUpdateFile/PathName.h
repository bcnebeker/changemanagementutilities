#pragma once
#include "WString.h"

class CPathName : public CWString
{
public:
	CPathName(const wchar_t *Init);
	CPathName(const CWString *ptr);
	CPathName();
	virtual ~CPathName();
	const wchar_t *	operator = (const wchar_t *ptr);
	const CWString * operator = (const CWString *ptr);
	operator const wchar_t *();
};

