#include "stdafx.h"
#include "PathName.h"


CPathName::CPathName(const wchar_t *Init) : CWString((size_t)MAX_PATH, Init)
{
}

CPathName::CPathName(const CWString *ptr) : CWString(ptr)
{
}

CPathName::CPathName() : CWString((size_t)MAX_PATH)
{
}

CPathName::~CPathName()
{
}

const wchar_t *	CPathName::operator = (const wchar_t *ptr)
{
	return CWString::operator=(ptr);
}

const CWString * CPathName::operator = (const CWString *ptr)
{
	return CWString::operator=(ptr);
}

CPathName::operator const wchar_t *()
{
	return CWString::operator const wchar_t *();
}