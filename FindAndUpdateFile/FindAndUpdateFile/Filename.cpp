#include "stdafx.h"
#include "Filename.h"


CFilename::CFilename() : CWString((size_t)FILENAME_MAX)
{
}

CFilename::CFilename(const wchar_t *Init) : CWString((size_t)FILENAME_MAX, Init)
{
}

CFilename::CFilename(const CWString *ptr) : CWString(ptr)
{
}


CFilename::~CFilename()
{
}

const wchar_t *	CFilename::operator = (const wchar_t *ptr)
{
	return CWString::operator=(ptr);
}

CFilename::operator const wchar_t *()
{
	return CWString::operator const wchar_t *();
}